import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import datasets, linear_model, model_selection
from sklearn.metrics import mean_squared_error
from sklearn import preprocessing

df = pd.read_csv("train.csv")

X = df.to_numpy()[:,2:]
#X = preprocessing.StandardScaler().fit_transform(X_unscaled)
Y = df.to_numpy()[:,1]

n_samples = int(len(Y))
n_features = np.shape(X)[1]
n_fold = 10

lambda_array = [0.1, 1, 10, 100, 1000]

RMSE = np.empty((len(lambda_array),n_fold))

for l in range(len(lambda_array)):
    
    kf = model_selection.KFold(n_splits=10)
    i = 0
    for train_index, test_index in kf.split(X):
        
        #Define Model
        regr = linear_model.Ridge(alpha=lambda_array[l])

        #Train Model
        regr.fit(X[train_index,:], Y[train_index])

        #compute RMSE
        Y_pred = regr.predict(X[test_index,:])
        RMSE[l,i] = mean_squared_error(Y[test_index],Y_pred)**0.5
        i += 1
RMSE = np.mean(RMSE, axis=1)

print(RMSE)

pd.DataFrame(RMSE).to_csv('sol.csv', index=False, header=False)

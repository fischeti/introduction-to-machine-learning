import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error

df = pd.read_csv("train.csv")

X = df.to_numpy()[:,2:]
Y = df.to_numpy()[:,1]

n_samples = int(len(Y))
n_features = np.shape(X)[1]
n_fold = 10

RMSE = np.empty((5,n_fold))

lambda_array = [0.1, 1, 10, 100, 1000]
    
#Define Model
regr = linear_model.RidgeCV(alphas=lambda_array, store_cv_values=True, cv=None)

#Train Model
regr.fit(X, Y)

#compute RMSE
print(regr.cv_values_)

RMSE = np.mean(RMSE, axis=1)

print(sum(RMSE))

pd.DataFrame(RMSE).to_csv('sol.csv', index=False, header=False)

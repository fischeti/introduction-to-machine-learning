import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import StandardScaler

#read csv file
df = pd.read_csv("train.csv")

#transform to numpy
X = df.to_numpy()[:,2:]
Y = df.to_numpy()[:,1]

#constants declarations
n_features = 21
n_samples = len(Y)

#split of train and test set
test_train_split = 800

#feature transformations
X_train_and_test = np.empty((n_samples,n_features))
X_train_and_test[:,:5] = X
X_train_and_test[:,5:10] = X*X
X_train_and_test[:,10:15] = np.exp(X)
X_train_and_test[:,15:20] = np.cos(X)
X_train_and_test[:,20] = 1

#data split
X_train = X_train_and_test[:test_train_split,:]
X_test = X_train_and_test[test_train_split:,:]

#targets split
Y_train = Y[:test_train_split]
Y_test = Y[test_train_split:]

#coefficient array to test with Cross-validation
alpha_array = np.geomspace(2**-4,2**4, num=9)

#Model definition
regr = linear_model.LassoCV(alphas=alpha_array, cv=10, fit_intercept=True)

#Train model
regr.fit(X_train, Y_train)

#predict test data
Y_pred = regr.predict(X_test)

#calculate RMSE of test set
RMSE = mean_squared_error(Y_pred, Y_test)**0.5
print("RMSE:", RMSE)
print("alpha:", regr.alpha_)

#plot
#plt.plot(np.arange(n_samples-test_train_split),Y_pred, color='red')
#plt.plot(np.arange(n_samples-test_train_split),Y_test, color='blue')
#plt.show()

#convert coefficients to csv
pd.DataFrame(regr.coef_).to_csv('sol.csv', index=False, header=False)

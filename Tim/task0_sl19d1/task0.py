import numpy as np
import pandas as pd
from sklearn.metrics import mean_squared_error

df = pd.read_csv("train.csv")

X = df.to_numpy()[:,2:]
Y = df.to_numpy()[:,1]

Y_pred = np.mean(X, axis=1)

RMSE = mean_squared_error(Y_pred, Y)**0.5

df_test = pd.read_csv("test.csv")

X_test = df_test.to_numpy()[:,1:]

Y_test = np.mean(X_test, axis=1)

pd.DataFrame({'Id' : np.arange(10000,12000), 'y' : Y_test}).to_csv('sol.csv', index=False)

import numpy as np
import pandas as pd
import keras
from keras.layers import Dense, BatchNormalization
from keras.models import Sequential
from keras import regularizers
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split

#read csv's for test
df_train = pd.read_csv("train.csv", index_col=0)
df_pred = pd.read_csv("test.csv", index_col=0)

#Convert to numpy arrays
X = df_train.to_numpy()[:,1:]
y = df_train.to_numpy()[:,0]

#Split into train and test set
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, shuffle=False)
X_public_test = df_pred.to_numpy()

#Neural Network Architecture
model = Sequential([
        Dense(60, activation='relu', kernel_regularizer=regularizers.l1(0.001), bias_regularizer=regularizers.l2(5), input_shape=(20,)),
        BatchNormalization(),
        Dense(40, activation='relu', kernel_regularizer=regularizers.l1(0.001), bias_regularizer=regularizers.l2(4)),
        BatchNormalization(),
        Dense(20, activation='relu', kernel_regularizer=regularizers.l1(0.001), bias_regularizer=regularizers.l2(3)),
        BatchNormalization(),
        Dense(10, activation='relu', kernel_regularizer=regularizers.l1(0.001), bias_regularizer=regularizers.l2(2)),
        BatchNormalization(),
        Dense(3, activation='softmax')
                    ])

#Define Optimizer
model.compile(optimizer='adam',
              loss='kullback_leibler_divergence',
              metrics=['accuracy'])

#Training
model.fit(X_train, keras.utils.to_categorical(y_train, num_classes=3), epochs=30)

y_test_pred = np.argmax(model.predict(X_test), axis=1)

#print histogram and accuracy score
unique, counts = np.unique(y_test_pred, return_counts=True)
print(dict(zip(unique, counts)))
print(accuracy_score(y_test, y_test_pred))

#predict results
y_public_pred = np.argmax(model.predict(X_public_test), axis=1)

#write to csv
pd.DataFrame({'Id' : np.arange(2000,5000), 'y' : y_public_pred.astype(int)}).to_csv('sol.csv', index=False)

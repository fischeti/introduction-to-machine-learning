import numpy as np
import pandas as pd
import keras
from keras.layers import Dense, BatchNormalization, Dropout
from keras.models import Sequential
from keras import regularizers
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split

#read csv's for test
df_train = pd.read_hdf("train.h5")
df_pred = pd.read_hdf("test.h5")

#Convert to numpy arrays
X = df_train.to_numpy()[:,1:]
y = df_train.to_numpy()[:,0]


#Split into train and test set
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, shuffle=False)
X_public_test = df_pred.to_numpy()


#Neural Network Architecture
model = Sequential([
        Dense(250, activation='relu', bias_regularizer=regularizers.l1_l2(5,5), kernel_initializer='random_uniform', bias_initializer='zeros', input_shape=(120,)),
        BatchNormalization(),
        Dropout(0.15),
        Dense(200, activation='relu', bias_regularizer=regularizers.l1_l2(4,4), kernel_initializer='random_uniform', bias_initializer='zeros'),
        BatchNormalization(),
        Dropout(0.15),
        Dense(200, activation='relu', bias_regularizer=regularizers.l1_l2(4,4), kernel_initializer='random_uniform', bias_initializer='zeros'),
        BatchNormalization(),
        Dropout(0.15),
        Dense(150, activation='relu', bias_regularizer=regularizers.l1_l2(4,4), kernel_initializer='random_uniform', bias_initializer='zeros'),
        BatchNormalization(),
        Dropout(0.3),
        Dense(100, activation='relu', bias_regularizer=regularizers.l1_l2(3,3), kernel_initializer='random_uniform', bias_initializer='zeros'),
        BatchNormalization(),
        Dropout(0.3),
        Dense(75, activation='relu', bias_regularizer=regularizers.l1_l2(2,2), kernel_initializer='random_uniform', bias_initializer='zeros'),
        BatchNormalization(),
        Dense(50, activation='relu', bias_regularizer=regularizers.l1_l2(1,1), kernel_initializer='random_uniform', bias_initializer='zeros'),
        BatchNormalization(),
        Dense(25, activation='relu'),
        BatchNormalization(),
        Dense(5, activation='softmax', kernel_initializer='random_uniform', bias_initializer='zeros')
                    ])

#Define Optimizer
lr = 0.01
beta_1 = 0.9
beta_2 = 0.999
epsilon = 10 ** (-8)
decay = 0.01
adamz = keras.optimizers.Adam(lr=lr, beta_1=beta_1, beta_2=beta_2, epsilon=epsilon, decay=decay, amsgrad=0)
model.compile(optimizer=adamz,
              loss='categorical_crossentropy',
              metrics=['accuracy'])

#Training
hist = model.fit(X_train, keras.utils.to_categorical(y_train, num_classes=5), batch_size=128, epochs=600)

#Predict Test set
y_test_pred = np.argmax(model.predict(X_test), axis=1)

#print histogram and accuracy score
unique, counts = np.unique(y_test_pred, return_counts=True)
print(dict(zip(unique, counts)))
print(accuracy_score(y_test, y_test_pred))


unique, counts = np.unique(y_test[y_test != y_test_pred], return_counts=True)
#print(dict(zip(unique, counts)))
#print(y_test[y_test != y_test_pred])
#print(y_test_pred[y_test != y_test_pred])

#predict results
y_public_pred = np.argmax(model.predict(X_public_test), axis=1)

#write to csv
pd.DataFrame({'Id' : np.arange(45324,53461), 'y' : y_public_pred.astype(int)}).to_csv('sol.csv', index=False)

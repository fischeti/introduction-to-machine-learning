import numpy as np
import pandas as pd
import keras
from keras.layers import Dense, BatchNormalization, Dropout
from keras.models import Sequential
from keras import regularizers
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split

#read csv's for test
df_train = pd.read_hdf("train.h5")
df_pred = pd.read_hdf("test.h5")

#Convert to numpy arrays
X = df_train.to_numpy()[:,1:]
y = df_train.to_numpy()[:,0]


#Split into train and test set
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, shuffle=False)
X_public_test = df_pred.to_numpy()

#Neural Network Architecture
model = Sequential([
        Dense(300, activation='relu', bias_regularizer=regularizers.l2(5),  input_shape=(120,)),
        BatchNormalization(),
        Dense(200, activation='relu', bias_regularizer=regularizers.l2(4)),
        BatchNormalization(),
        Dense(100, activation='relu', bias_regularizer=regularizers.l2(3)),
        BatchNormalization(),
        Dense(75, activation='relu', bias_regularizer=regularizers.l2(2)),
        BatchNormalization(),
        Dense(50, activation='relu', bias_regularizer=regularizers.l2(1)),
        BatchNormalization(),
        Dense(25, activation='relu'),
        BatchNormalization(),
        Dense(5, activation='softmax')
                    ])

#Define Optimizer
model.compile(optimizer='adam',
              loss='categorical_hinge',
              metrics=['accuracy'])

#Training
hist = model.fit(X_train, keras.utils.to_categorical(y_train, num_classes=5), epochs=30)

#Predict Test set
y_test_pred = np.argmax(model.predict(X_test), axis=1)

#print histogram and accuracy score
unique, counts = np.unique(y_test_pred, return_counts=True)
print(dict(zip(unique, counts)))
print(accuracy_score(y_test, y_test_pred))

#predict results
y_public_pred = np.argmax(model.predict(X_public_test), axis=1)

#write to csv
pd.DataFrame({'Id' : np.arange(45324,53461), 'y' : y_public_pred.astype(int)}).to_csv('sol.csv', index=False)
